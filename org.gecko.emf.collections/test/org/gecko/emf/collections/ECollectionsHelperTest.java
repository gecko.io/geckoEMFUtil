package org.gecko.emf.collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.gecko.collection.CollectionFactory;
import org.gecko.collection.FeaturePath;
import org.gecko.collection.helper.ECollectionsHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ECollectionsHelperTest {

	private ResourceSet resourceSet = null;
	private Resource resource = null;
	private EPackage ecollPackage = null;
	
	@Before
	public void setup() throws IOException {
		resourceSet = new ResourceSetImpl();
		resourceSet.getPackageRegistry().put(EcorePackage.eNS_URI, EcorePackage.eINSTANCE);
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new XMIResourceFactoryImpl());
		resource = resourceSet.createResource(URI.createURI("emfcollection.ecore"));
		assertNotNull(resource);
		InputStream collStream = getClass().getResourceAsStream("emfcollection.ecore");
		assertNotNull(collStream);
		resource.load(collStream, null);
		EObject c = resource.getContents().get(0);
		assertTrue(c instanceof EPackage);
		ecollPackage = (EPackage) c;
	}
	
	@After
	public void after() {
		resource.getContents().clear();
		resourceSet.getResources().clear();
		resourceSet = null;
	}
	
	@Test
	public void testGetValue() throws IOException {
		assertNotNull(ecollPackage);
		
		assertNull(ECollectionsHelper.getFeaturePathValue(null, null));
		assertNull(ECollectionsHelper.getFeaturePathValue(null, ecollPackage));
		
		FeaturePath fp = CollectionFactory.eINSTANCE.createFeaturePath();
		assertNull(ECollectionsHelper.getFeaturePathValue(fp, null));
		assertNull(ECollectionsHelper.getFeaturePathValue(fp, ecollPackage));
		
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__NS_PREFIX);
		assertEquals(1, ECollectionsHelper.getFeaturePathValue(fp, ecollPackage).size());
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.ESTRUCTURAL_FEATURE__DERIVED);
		assertNull(ECollectionsHelper.getFeaturePathValue(fp, ecollPackage));
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__NS_PREFIX);
		fp.getFeature().add(EcorePackage.Literals.ESTRUCTURAL_FEATURE__DEFAULT_VALUE);
		assertNull(ECollectionsHelper.getFeaturePathValue(fp, ecollPackage));
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__ECLASSIFIERS);
		fp.getFeature().add(EcorePackage.Literals.ENAMED_ELEMENT__NAME);
		List<Object> result = ECollectionsHelper.getFeaturePathValue(fp, ecollPackage);
		assertEquals(9, result.size());
		assertTrue(result.get(0) instanceof String);
		// check some class names from the model
		assertTrue(result.contains("EIterable"));
		assertTrue(result.contains("FeaturePath"));
		
	}
	
	@Test
	public void testGetValueSelf() throws IOException {
		assertNotNull(ecollPackage);
		
		FeaturePath fp = CollectionFactory.eINSTANCE.createFeaturePath();
		assertEquals(0, fp.getValue(null).size());
		assertEquals(0, fp.getValue(ecollPackage).size());
		
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__NS_PREFIX);
		assertEquals(1, fp.getValue(ecollPackage).size());
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.ESTRUCTURAL_FEATURE__DERIVED);
		assertEquals(0, fp.getValue(ecollPackage).size());
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__NS_PREFIX);
		fp.getFeature().add(EcorePackage.Literals.ESTRUCTURAL_FEATURE__DEFAULT_VALUE);
		assertEquals(0, fp.getValue(ecollPackage).size());
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__ECLASSIFIERS);
		fp.getFeature().add(EcorePackage.Literals.ENAMED_ELEMENT__NAME);
		List<Object> result = fp.getValue(ecollPackage);
		assertEquals(9, result.size());
		assertTrue(result.get(0) instanceof String);
		// check some class names from the model
		assertTrue(result.contains("EIterable"));
		assertTrue(result.contains("FeaturePath"));
		
	}
	
	@Test
	public void testValidatePath() throws IOException {
		assertNotNull(ecollPackage);
		EClass ecollPackageClass = ecollPackage.eClass();
		
		assertFalse(ECollectionsHelper.validateFeaturePath(null, null));
		assertFalse(ECollectionsHelper.validateFeaturePath(null, ecollPackageClass));
		
		FeaturePath fp = CollectionFactory.eINSTANCE.createFeaturePath();
		assertFalse(ECollectionsHelper.validateFeaturePath(fp, null));
		assertFalse(ECollectionsHelper.validateFeaturePath(fp, ecollPackageClass));
		
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__ECLASSIFIERS);
		assertTrue(ECollectionsHelper.validateFeaturePath(fp, ecollPackageClass));
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.ESTRUCTURAL_FEATURE__DERIVED);
		assertFalse(ECollectionsHelper.validateFeaturePath(fp, ecollPackageClass));
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__NS_PREFIX);
		fp.getFeature().add(EcorePackage.Literals.ESTRUCTURAL_FEATURE__DEFAULT_VALUE);
		assertFalse(ECollectionsHelper.validateFeaturePath(fp, ecollPackageClass));
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__ECLASSIFIERS);
		fp.getFeature().add(EcorePackage.Literals.ENAMED_ELEMENT__NAME);
		assertTrue(ECollectionsHelper.validateFeaturePath(fp, ecollPackageClass));
		
	}
	
	@Test
	public void testValidatePathSelf() throws IOException {
		assertNotNull(ecollPackage);
		
		FeaturePath fp = CollectionFactory.eINSTANCE.createFeaturePath();
		assertFalse(fp.isValid(null));
		assertFalse(fp.isValid(ecollPackage));
		
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__ECLASSIFIERS);
		assertTrue(fp.isValid(ecollPackage));
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.ESTRUCTURAL_FEATURE__DERIVED);
		assertFalse(fp.isValid(ecollPackage));
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__NS_PREFIX);
		fp.getFeature().add(EcorePackage.Literals.ESTRUCTURAL_FEATURE__DEFAULT_VALUE);
		assertFalse(fp.isValid(ecollPackage));
		
		fp = CollectionFactory.eINSTANCE.createFeaturePath();
		fp.getFeature().add(EcorePackage.Literals.EPACKAGE__ECLASSIFIERS);
		fp.getFeature().add(EcorePackage.Literals.ENAMED_ELEMENT__NAME);
		assertTrue(fp.isValid(ecollPackage));
		
	}

}
