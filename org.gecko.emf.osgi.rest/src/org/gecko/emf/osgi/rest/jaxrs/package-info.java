@org.osgi.service.jaxrs.whiteboard.annotations.RequireJaxrsWhiteboard
@org.gecko.emf.osgi.annotation.require.RequireEMF
@org.osgi.annotation.bundle.Capability(namespace = "gecko.rest.addon", name = "messagebody.emf", version = "1.0.0")
package org.gecko.emf.osgi.rest.jaxrs;
